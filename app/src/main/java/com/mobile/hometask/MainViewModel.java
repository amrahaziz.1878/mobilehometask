package com.mobile.hometask;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.ViewModel;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class MainViewModel extends AndroidViewModel {

    public SharedPreferences getSharedPreferences() {
        return sharedPreferences;
    }

    private final SharedPreferences sharedPreferences;

    public MainViewModel(@NonNull Application application) {
        super(application);
        sharedPreferences = application.getSharedPreferences("mobileHomeTask", Context.MODE_PRIVATE);
    }

    public void setTransportation(ArrayList<TransportationDto> transportation) {
        this.transportation.clear();
        this.transportation.addAll(transportation);
    }

    public ArrayList<TransportationDto> getTransportation() {
        return transportation;
    }

    private final ArrayList<TransportationDto> transportation = new ArrayList<>();

    public ArrayList<TransportationDto> fetchDto() {
        if (sharedPreferences.contains("data")) {
            Type type = new TypeToken<ArrayList<TransportationDto>>() {
            }.getType();
            return new Gson().fromJson(sharedPreferences.getString("data", ""), type);
        }
        ArrayList<TransportationDto> list = new ArrayList<>();
        list.add(new TransportationDto("Taxi", 20.0, "Ground lines", 0, 0L));
        list.add(new TransportationDto("Limousine", 35.0, "Ground lines", 0, 0L));
        list.add(new TransportationDto("Bus", 10.0, "Ground lines", 0, 0L));
        list.add(new TransportationDto("Train", 20.0, "Ground lines", 0, 0L));
        list.add(new TransportationDto("Passengers plane", 50.0, "Air lines", 0, 0L));
        list.add(new TransportationDto("Cargo plane", 70.0, "Air lines", 0, 0L));

        return list;
    }
}
