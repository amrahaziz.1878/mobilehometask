package com.mobile.hometask;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.mobile.hometask.databinding.ActivityMainBinding;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;

    private MainViewModel viewModel;

    private TransportationAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        viewModel = new ViewModelProvider(this).get(MainViewModel.class);
        setRecyclerView();

        setTimer();

    }

    private void setTimer() {
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(() -> {
                            ArrayList<TransportationDto> newList = viewModel.getTransportation();
                            for (int i=0; i<newList.size(); i++){
                                TransportationDto transportationDto = newList.get(i);
                                if (transportationDto.getCountTime() != 0L && transportationDto.getCountTime()<System.currentTimeMillis()){
                                    transportationDto.setCountTime(0L);
                                    transportationDto.setCount(transportationDto.getCount()+100);
                                }
                            }
                            adapter.setTransportationList(newList);
                            viewModel.getSharedPreferences().edit().putString("data", new Gson().toJson(newList)).apply();
                        }
                );
            }
        }, 0, 1000);
    }

    private void setRecyclerView() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        binding.recyclerView.setLayoutManager(linearLayoutManager);
        viewModel.setTransportation(viewModel.fetchDto());
        adapter = new TransportationAdapter(viewModel.getTransportation(), this, index -> {
            viewModel.getTransportation().get(index).setCountTime(System.currentTimeMillis() + 120000);
            adapter.notifyItemChanged(index);
        });
        binding.recyclerView.setAdapter(adapter);
    }
}