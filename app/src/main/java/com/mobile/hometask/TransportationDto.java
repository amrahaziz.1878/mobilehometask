package com.mobile.hometask;

public class TransportationDto {

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    private String name;
    private Double price;
    private String type;

    public Long getCountTime() {
        return countTime;
    }

    public void setCountTime(Long countTime) {
        this.countTime = countTime;
    }

    private int count;
    private Long countTime;

    public TransportationDto(String name, Double price, String type, int count, Long countTime) {
        this.name = name;
        this.price = price;
        this.type = type;
        this.count = count;
        this.countTime = countTime;
    }
}
