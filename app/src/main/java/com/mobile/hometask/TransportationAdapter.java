package com.mobile.hometask;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

interface OnItemClickListener {
    void onItemClick(int index);
}

public class TransportationAdapter extends RecyclerView.Adapter<TransportationAdapter.TransportViewHolder> {
    public void setTransportationList(ArrayList<TransportationDto> transportationList) {
        this.transportationList = transportationList;
        this.notifyDataSetChanged();
    }

    ArrayList<TransportationDto> transportationList;
    Context context;
    OnItemClickListener clickListener;

    public TransportationAdapter(ArrayList<TransportationDto> transportationList, Context context, OnItemClickListener clickListener) {
        this.transportationList = transportationList;
        this.context = context;
        this.clickListener = clickListener;
    }

    @NonNull
    @Override
    public TransportViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.transport_item_list_view, parent, false);

        return new TransportViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TransportViewHolder holder, int position) {
        holder.bind(transportationList.get(position), context, clickListener);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return transportationList.size();
    }


    static class TransportViewHolder extends RecyclerView.ViewHolder {
        public TransportViewHolder(View binding) {
            super(binding);
        }

        public void bind(TransportationDto transportationDto, Context context, OnItemClickListener clickListener) {

            ((TextView) itemView.findViewById(R.id.name)).setText(transportationDto.getName());
            ((TextView) itemView.findViewById(R.id.count)).setText(String.valueOf(transportationDto.getCount()));
            ((TextView) itemView.findViewById(R.id.type)).setText(transportationDto.getType());
            if (transportationDto.getCountTime() > System.currentTimeMillis()) {
                ((TextView) itemView.findViewById(R.id.price)).setText("Building");
                ((TextView) itemView.findViewById(R.id.price)).setTextColor(ContextCompat.getColor(context, R.color.red));
                itemView.findViewById(R.id.get_it).setVisibility(View.GONE);
            } else {
                ((TextView) itemView.findViewById(R.id.price)).setTextColor(ContextCompat.getColor(context, R.color.black));
                ((TextView) itemView.findViewById(R.id.price)).setText(context.getString(R.string.price_pattern, transportationDto.getPrice().toString()));
                itemView.findViewById(R.id.get_it).setVisibility(View.VISIBLE);
            }
            itemView.findViewById(R.id.get_it).setOnClickListener(view -> {
                clickListener.onItemClick(getAdapterPosition());
            });
        }
    }
}
